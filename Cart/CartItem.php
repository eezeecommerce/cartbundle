<?php

namespace eezeecommerce\CartBundle\Cart;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use eezeecommerce\CartBundle\Core\EntityInterface;
use eezeecommerce\ProductBundle\Entity\Options;
use eezeecommerce\ProductBundle\Entity\Variants;

class CartItem
{
    /**
     * @var EntityInterface
     */
    protected $entity;

    /**
     * @var integer
     */
    protected $quantity;

    /**
     * Selected options for order line chosen when added to cart
     *
     * @var ArrayCollection|null
     */
    protected $options;

    /**
     * Integer of selected variant ID
     *
     * @var integer
     */
    protected $variant;

    /**
     * Float of discount for specific product line
     *
     * @var float
     */
    protected $discount = 0.00;

    /**
     * CartItem constructor.
     *
     * @param EntityInterface $entity   Injects Entity Interface for product collections
     * @param integer         $quantity Injects chosen quantity of product wanted
     */
    public function __construct(EntityInterface $entity, $quantity)
    {
        if (!intval($quantity)) {
            $quantity = 1;
        }

        $this->entity = $entity;

        $this->quantity = $quantity;
    }

    /**
     * Sets array of options chosen when added to cart
     *
     * @param array $optionsArray Array of Options
     *
     * @return void
     */
    public function setOptions(array $optionsArray)
    {
        $options = $this->entity->getOptions();

        foreach ($options as $key => $option) {
            if (array_key_exists($option->getId(), $optionsArray)) {
                if (!empty($value = $optionsArray[$option->getId()])) {
                    $option->setValue($value);
                }
            }
        }
    }

    /**
     * Sets relevant files for product when added to cart
     *
     * @param array $filesArray Array of Files
     *
     * @return void
     */
    public function setFiles(array $filesArray)
    {
        $options = $this->entity->getOptions();

        if (null === $options) {
            return;
        }

        foreach ($options as $key => $option) {
            if (array_key_exists($option->getId(), $filesArray)) {
                if (!empty($value = $filesArray[$option->getId()])) {
                    $option->setValue($value->getImageName());
                }
            }
        }
    }

    /**
     * Returns Selected variant for product
     *
     * @return Variants|null
     */
    public function getChosenVariant()
    {
        if (count($variants = $this->getVariants()) > 0) {
            foreach ($variants as $variant) {
                if ($this->variant == $variant->getId()) {
                    return $variant;
                }
            }
        }
    }

    /**
     * Set Variant Choice
     *
     * @param int $variant Variant Id
     *
     * @return void
     */
    public function setVariant($variant)
    {
        $this->variant = $variant;
    }

    /**
     * @return ArrayCollection|Variants
     */
    public function getVariants()
    {
        return $this->entity->getVariants();
    }

    /**
     * @return ArrayCollection|Options
     */
    public function getOptions()
    {
        return $this->entity->getOptions();
    }

    /**
     * Return Entity
     *
     * @return EntityInterface
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Get Individual Price of order Line Item
     * @return float
     */
    public function getPrice()
    {
        $price = $this->getEntity()->getBasePrice();

        if (count($variants = $this->getVariants()) > 0) {
            foreach ($variants as $variant) {
                if ($this->variant == $variant->getId()) {
                    $price = $variant->getBasePrice();
                    break;
                }
            }
        }

        return $price;
    }

    /**
     * Override quantity originally set in constructor
     *
     * @param int $qty quantity
     *
     * @return void
     */
    public function setQuantity($qty)
    {
        $this->quantity = $qty;
    }

    /**
     * Return Entity Quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Returns weight of orderline
     * @return float
     */
    public function getWeight()
    {
        return $this->getEntity()->getWeight() * $this->quantity;
    }

    /**
     * Gets subtotal (inc discount) of orderline
     * @return float|number
     */
    public function getSubTotal()
    {

        $price = $this->getPrice();

        if (count($this->getOptions()) > 0) {
            $subtotal = [];
            foreach ($this->getOptions() as $option) {
                if (null === $option->getValue()) {
                    continue;
                }

                $optionPrice = $option->getBasePrice();

                if ($option->getOptionChoices()->count() > 0) {
                    foreach ($option->getOptionChoices() as $choice) {
                        if ($option->getValue() == $choice->getName()
                            && $choice->getBasePrice() !== 0.0000
                        ) {
                            $optionPrice = $choice->getBasePrice();
                        }
                    }
                }

                array_push($subtotal, $optionPrice);
            }
            $price += array_sum($subtotal);
        }

        return ($price * $this->getQuantity());
    }

    /**
     * Get Product Image
     *
     * @return array
     */
    public function getImage()
    {
        foreach ($this->entity->getImage() as $image) {
            return $image->getImageName();
        }

        $images = $this->entity->getImage();
        if (!$images instanceof PersistentCollection) {

        }

        return $images->getValues();
    }

    /**
     * Set discount amount for order line
     *
     * @param float $discount Discount amount
     *
     * @return void
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * Get discount amount for orderline
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

}