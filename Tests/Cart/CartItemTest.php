<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09/05/16
 * Time: 13:49
 */

namespace eezeecommerce\CartBundle\Tests\Cart;


use eezeecommerce\CartBundle\Cart\CartItem;

class CartItemTest extends \PHPUnit_Framework_TestCase
{
    public function testInvalidQtyThrowsInvalidException()
    {
        $entity = $this
            ->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->disableOriginalConstructor()
            ->getMock();

        $quantity = 0.5;

        $cart = new CartItem($entity, $quantity);

        $this->assertEquals($cart->getQuantity(), 1);
    }

    public function testEntityReturnsInstanceOfEntityInterface()
    {
        $entity = $this
            ->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->disableOriginalConstructor()
            ->getMock();

        $quantity = 1;

        $cartItem = new CartItem($entity, $quantity);

        $this->assertEquals($entity, $cartItem->getEntity());
    }



    public function testEntityReturnsValidQuantity()
    {
        $entity = $this
            ->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->disableOriginalConstructor()
            ->getMock();

        $quantity = 1;

        $cartItem = new CartItem($entity, $quantity);

        $this->assertEquals($quantity, $cartItem->getQuantity());
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid item quantity: asd
     */
    public function testConstructWithStringThrowsInvalidArgumentException()
    {
        $entity = $this
            ->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->disableOriginalConstructor()
            ->getMock();

        $quantity = "asd";

        $cart = new CartItem($entity, $quantity);
        $this->assertEquals($cart->getQuantity(), 1);
    }

    public function testSettingFileWhenNoOptionsExist()
    {
        $entity = $this
            ->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->disableOriginalConstructor()
            ->getMock();

        $entity->expects($this->once())
            ->method("getOptions")
            ->willReturn(null);

        $quantity = 1;

        $cartItem = new CartItem($entity, $quantity);

        $this->assertNull($cartItem->setFiles(array()));
    }
}