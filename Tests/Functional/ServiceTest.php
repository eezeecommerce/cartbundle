<?php

namespace eezeecommerce\CartBundle\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;

class ServiceTest extends WebTestCase
{
    /**
     * @var Container
     */
    protected $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testCartServiceExists()
    {
        $this->client->request("GET", "http://eezeebundles.local/cart");
        $container =  $this->client->getContainer();

        $this->assertTrue($container->has("eezeecommerce.cart"));
    }
}