<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17/05/16
 * Time: 11:47
 */

namespace eezeecommerce\CartBundle\Tests\Functional;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FileSystemTest extends WebTestCase
{

    public function testUploadsDirectoryExists()
    {
        $kernel = static::createKernel();
        $dir = $kernel->getRootDir();

        $this->assertFileExists($dir."/../web/uploads");
        $this->assertFileExists($dir."/../web/files");

        $this->assertEquals(decoct(0777), decoct(fileperms($dir."/../web/uploads") & 0777));
        $this->assertEquals(decoct(0777), decoct(fileperms($dir."/../web/files") & 0777));
    }

}