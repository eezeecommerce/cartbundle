<?php

namespace eezeecommerce\CartBundle\Tests\Helper;


use eezeecommerce\CartBundle\Core\EntityInterface;

/**
 * Class Entity
 * @package Helper
 *
 * @codeCoverageIgnore
 */
class Entity implements EntityInterface
{

    /**
     * Returns Id
     *
     * @return int
     */
    public function getId()
    {
        return 1;
    }

    /**
     * Returns Base Price
     *
     * @return float
     */
    public function getBasePrice()
    {
        return 1.2;
    }

    public function getOptions()
    {

    }

    public function getVariants()
    {

    }
}