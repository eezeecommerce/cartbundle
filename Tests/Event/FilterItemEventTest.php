<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/05/16
 * Time: 08:40
 */

namespace eezeecommerce\CartBundle\Tests\Event;


use eezeecommerce\CartBundle\Event\FilterItemEvent;
use Symfony\Component\HttpFoundation\Response;

class FilterItemEventTest extends \PHPUnit_Framework_TestCase
{
    public function testEventCartItemContainsEntity()
    {
        $helper = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->disableOriginalConstructor()
            ->getMock();

        $cart = $this->getMockBuilder("eezeecommerce\CartBundle\Cart\CartItem")
            ->disableOriginalConstructor()
            ->getMock();

        $cart->expects($this->once())
            ->method("getEntity")
            ->will($this->returnValue($helper));

        $event = new FilterItemEvent($cart);

        $this->assertEquals($helper, $event->getEntity());

    }

    public function testEventReturnsCartItem()
    {
        $cart = $this->getMockBuilder("eezeecommerce\CartBundle\Cart\CartItem")
            ->disableOriginalConstructor()
            ->getMock();

        $event = new FilterItemEvent($cart);

        $this->assertEquals($cart, $event->getItem());
    }

    public function testSettingResponse()
    {
        $cart = $this->getMockBuilder("eezeecommerce\CartBundle\Cart\CartItem")
            ->disableOriginalConstructor()
            ->getMock();

        $event = new FilterItemEvent($cart);

        $response = $this->getMockBuilder(Response::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event->setResponse($response);

        $this->assertEquals($response, $event->getResponse());
    }

    public function testEventReturnsValidQuantityFromCartItem()
    {
        $quantity = 3;

        $cart = $this->getMockBuilder("eezeecommerce\CartBundle\Cart\CartItem")
            ->disableOriginalConstructor()
            ->getMock();

        $cart->expects($this->once())
            ->method("getQuantity")
            ->will($this->returnValue($quantity));

        $event = new FilterItemEvent($cart);

        $this->assertEquals($quantity, $event->getQuantity());
    }
}