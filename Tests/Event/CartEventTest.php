<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24/05/16
 * Time: 16:49
 */

namespace eezeecommerce\CartBundle\Tests\Event;

use eezeecommerce\CartBundle\Core\CartManager;
use eezeecommerce\CartBundle\Event\CartEvent;


class CartEventTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructorRequiresCartManager()
    {
        $manager = $this->getMockBuilder(CartManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event = new CartEvent($manager);

        $this->assertEquals($manager, $event->getCart());
    }
}