<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/05/16
 * Time: 09:10
 */

namespace eezeecommerce\CartBundle\Tests\Event;


use eezeecommerce\CartBundle\Event\FilterItemsEvent;

class FilterItemsEventTest extends \PHPUnit_Framework_TestCase
{
    public function testGetItemsReturnsArray()
    {
        $helper = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->disableOriginalConstructor()
            ->getMock();

        $helper->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $item = $this->getMockBuilder("eezeecommerce\CartBundle\Cart\CartItem")
            ->disableOriginalConstructor()
            ->getMock();

        $item->expects($this->any())
            ->method("getEntity")
            ->will($this->returnValue($helper));

        $items[$item->getEntity()->getId()] = $item;

        $helper->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(2));

        $item->expects($this->any())
            ->method("getEntity")
            ->will($this->returnValue($helper));

        $items[$item->getEntity()->getId()] = $item;

        $event = new FilterItemsEvent($items);

        $this->assertTrue(is_array($event->getItems()));

        $this->assertEquals($items, $event->getItems());
    }
}