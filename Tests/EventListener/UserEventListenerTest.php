<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 22/08/16
 * Time: 14:25
 */

namespace EventListener;


use eezeecommerce\CartBundle\Core\CartManager;
use eezeecommerce\CartBundle\EventListener\UserEventListener;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class UserEventListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testClassImplementsEventSubscriber()
    {
        $rc = new \ReflectionClass(UserEventListener::class);

        $this->assertTrue($rc->implementsInterface(EventSubscriberInterface::class));
    }

    public function testConstructorAcceptsCartManagerAsArgument()
    {
        $cartManager = $this->getMockBuilder(CartManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        new UserEventListener($cartManager);
    }

    public function testStaticContainerReturnsValidArray()
    {
        $result = UserEventListener::getSubscribedEvents();

        $this->assertTrue(is_array($result));
        $this->assertTrue(array_key_exists(SecurityEvents::INTERACTIVE_LOGIN, $result));
        $this->assertEquals(array("onUserLogin", 0), $result[SecurityEvents::INTERACTIVE_LOGIN]);
    }

    public function testCallingOnUserLoginRunsManagerUpdatePricingOnce()
    {
        $cartManager = $this->getMockBuilder(CartManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $cartManager->expects($this->once())
            ->method("updatePricing")
            ->will($this->returnValue(null));

        $event = new UserEventListener($cartManager);

        $mockEvent = $this->getMockBuilder(InteractiveLoginEvent::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event->onUserLogin($mockEvent);
    }
}