<?php

namespace eezeecommerce\CartBundle\Tests\Storage;


use eezeecommerce\CartBundle\Storage\SessionStorage;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SessionStorageTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testSessionStorageImplementsStorageInterface()
    {
        $sessionStorage = new \ReflectionClass("eezeecommerce\CartBundle\Storage\SessionStorage");

        $this->assertTrue($sessionStorage->implementsInterface("eezeecommerce\CartBundle\Storage\StorageInterface"));
    }

    public function testAddToCartSavesData()
    {
        $session = $this->client->getContainer()->get("session");

        $storage = new SessionStorage($session);

        $key = "test";

        $data = "test data";

        $storage->set($key, $data);

        $this->assertEquals($data, $storage->get($key));
    }

    public function testGetCartOnItemThatDoesntExistReturnsNull()
    {
        $session = $this->client->getContainer()->get("session");

        $storage = new SessionStorage($session);

        $this->assertEquals(null, $storage->get("key that does not exist"));
    }

    public function testGetStorageHasReturnsBoolean()
    {
        $session = $this->client->getContainer()->get("session");

        $storage = new SessionStorage($session);

        $this->assertEquals(false, $storage->has("key"));

        $key = "test";

        $data = "test data";

        $storage->set($key, $data);

        $this->assertTrue($storage->has($key));
    }

    public function testGetAllReturnsExactArray()
    {
        $session = $this->client->getContainer()->get("session");

        $storage = new SessionStorage($session);

        $array = [1 => "1", 2 => "2", 3 => "asd", 4 => "wer"];

        $storage->set(1, "1");
        $storage->set(2, "2");
        $storage->set(3, "asd");
        $storage->set(4, "wer");


        $this->assertEquals($array, $storage->all());
    }

    public function testRemoveItemReturnsCorrectArray()
    {
        $session = $this->client->getContainer()->get("session");

        $storage = new SessionStorage($session);

        $array = [1 => "1", 3 => "asd", 4 => "wer"];

        $storage->set(1, "1");
        $storage->set(2, "2");
        $storage->set(3, "asd");
        $storage->set(4, "wer");

        $storage->remove(2);

        $this->assertEquals($array, $storage->all());
    }

    public function testClearCartReturnsNullOnGet()
    {
        $session = $this->client->getContainer()->get("session");

        $storage = new SessionStorage($session);

        $storage->set(1, "1");
        $storage->set(2, "2");
        $storage->set(3, "asd");
        $storage->set(4, "wer");

        $storage->clear();

        $this->assertEquals([], $storage->all());
    }
}