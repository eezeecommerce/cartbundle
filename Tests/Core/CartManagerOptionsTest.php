<?php

namespace eezeecommerce\CartBundle\Tests\Core;


use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use eezeecommerce\CartBundle\Storage\SessionStorage;
use eezeecommerce\CartBundle\Core\CartManager;

class CartManagerOptionsTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testAddItemWithOptionReturnsCorrectPrice()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));

        $option = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Options")
            ->disableOriginalConstructor()
            ->getMock();

        $option->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));

        $option->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $option->expects($this->any())
            ->method("getOptionChoices")
            ->will($this->returnValue(new ArrayCollection()));

        $option->expects($this->any())
            ->method("getValue")
            ->will($this->returnValue("asd"));

        $entity->expects($this->any())
            ->method("getOptions")
            ->will($this->returnValue([$option->getId() => $option]));


        $array = [];
        $array[0] = "njasdjh";
        $array[1] = "asd";

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2, $array);

        $this->assertEquals(11.96, $manager->total());
    }

    public function testAddProductWithOptionChoices()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));

        $optionChoice1 = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\OptionChoices")
            ->disableOriginalConstructor()
            ->getMock();

        $optionChoice1->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $optionChoice1->expects($this->any())
            ->method("getValue")
            ->will($this->returnValue("asd"));

        $optionChoice1->expects($this->any())
            ->method("getName")
            ->will($this->returnValue("asd"));

        $optionChoice1->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(5.00));

        $array = new ArrayCollection();

        $array->add($optionChoice1);

        $option = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Options")
            ->disableOriginalConstructor()
            ->getMock();

        $option->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));

        $option->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $option->expects($this->any())
            ->method("getOptionChoices")
            ->will($this->returnValue($array));

        $option->expects($this->any())
            ->method("getValue")
            ->will($this->returnValue("asd"));

        $entity->expects($this->any())
            ->method("getOptions")
            ->will($this->returnValue([$option->getId() => $option]));


        $array = [];
        $array[0] = "njasdjh";
        $array[1] = "asd";

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2, $array);

        $this->assertEquals(15.98, $manager->total());
    }

    public function testCartWithNoOptions()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2, null);

        $this->assertEquals(5.98, $manager->total());
    }
}