<?php

namespace eezeecommerce\CartBundle\Tests\Core;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use eezeecommerce\CartBundle\Storage\SessionStorage;
use eezeecommerce\CartBundle\Core\CartManager;

class CartManagerVariantTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testAddItemWithNoOptionsAndNoVariants()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2, null, null);

        $this->assertEquals(5.98, $manager->total());
    }

    public function testAddItemWithVariantsUsesVariantPriceOverProductPrice()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));

        $variant1 = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Variants")
                    ->disableOriginalConstructor()
            ->getMock();

        $variant2 = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Variants")
            ->disableOriginalConstructor()
            ->getMock();

        $variant1->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $variant2->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(2));

        $variant1->expects($this->any())
            ->method("getValue")
            ->will($this->returnValue("a"));

        $variant2->expects($this->any())
            ->method("getValue")
            ->will($this->returnValue("a"));

        $variant1->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(3.50));

        $variant2->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(8.00));

        $array = new ArrayCollection();
        $array->add($variant1);
        $array->add($variant2);

        $entity->expects($this->any())
            ->method("getVariants")
            ->will($this->returnValue($array));

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 1, null, null, 2);

        $this->assertEquals(8, $manager->total());
    }
}