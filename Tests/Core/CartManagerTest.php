<?php

namespace eezeecommerce\CartBundle\Tests\Core;


use eezeecommerce\CartBundle\Core\CartManager;
use eezeecommerce\CartBundle\Storage\SessionStorage;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use eezeecommerce\CartBundle\Cart\CartItem;

class CartManagerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testCartManagerImplementsInterface()
    {
        $manager = new \ReflectionClass("eezeecommerce\CartBundle\Core\CartManager");
        $this->assertTrue($manager->implementsInterface("eezeecommerce\CartBundle\Core\CartManagerInterface"));
    }

    public function testConstructorAcceptsTwoArguements()
    {
        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = $this->getMockBuilder("eezeecommerce\CartBundle\Storage\StorageInterface")
            ->disableOriginalConstructor()
            ->getMock();

        new CartManager($dispatcher, $storage);
    }

    public function testAddItemReturnsValidObject()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
        ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $expected = new CartItem($entity, 2);

        $this->assertEquals([0 => $expected], $manager->getItems());
    }

    public function testHasItemReturnBoolean()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $this->assertTrue($manager->hasItem(0));
    }

    public function testCountItemsReturnsTwo()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity2 = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity2->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(2));

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $manager->addItem($entity2, 1);

        $this->assertEquals(2, $manager->count());
    }

    public function testRemoveCartItemByIdReturnsEmptyArray()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $manager->removeItem(0);

        $this->assertEmpty($manager->getItems());
    }

    public function testRemoveCartItemWithInvalidId()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $manager = new CartManager($dispatcher, $storage);

        $this->assertEquals(false, $manager->removeItem(2));
    }

    public function testCartEmptyReturnsTrue()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $manager = new CartManager($dispatcher, $storage);

        $this->assertTrue($manager->isEmpty());
    }

    public function testCartClearReturnsEmptyArray()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $this->assertEquals(1, $manager->count());

        $manager->clear();

        $this->assertEmpty($manager->getItems());
    }

    public function testCartGetTotalReturnsCorrectValue()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));

        $entity2 = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity2->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(2));

        $entity2->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(3.00));


        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $manager->addItem($entity2, 1);

        $this->assertEquals(8.98, $manager->total());
    }

    public function testEmptyArrayTotalEqualsZero()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $manager = new CartManager($dispatcher, $storage);

        $this->assertEquals(0, $manager->total());
    }

    public function testUpdateWithInvalidIdDoesReturnsFalse()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));


        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $this->assertNotTrue($manager->updateQty(5, 2));
    }

    public function testUpdateWithLessThanOneRemovesItem()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));


        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $this->assertTrue($manager->hasItem(0));

        $manager->updateQty(0, 0);

        $this->assertNotTrue($manager->hasItem(0));
    }

    public function testUpdateQuantitySuccessfullyUpdatesItemQuantity()
    {
        $session = $this->client->getContainer()->get("session");

        $dispatcher = $this->getMockBuilder("Symfony\Component\EventDispatcher\EventDispatcherInterface")
            ->disableOriginalConstructor()
            ->getMock();

        $storage = new SessionStorage($session);

        $entity = $this->getMockBuilder("eezeecommerce\CartBundle\Tests\Helper\Entity")
            ->getMock();

        $entity->expects($this->any())
            ->method("getId")
            ->will($this->returnValue(1));

        $entity->expects($this->any())
            ->method("getBasePrice")
            ->will($this->returnValue(2.99));


        $manager = new CartManager($dispatcher, $storage);

        $manager->addItem($entity, 2);

        $item = $manager->getItems();

        $this->assertEquals(2, $item[0]->getQuantity());

        $manager->updateQty(0, 3);

        $item = $manager->getItems();

        $this->assertEquals(3, $item[0]->getQuantity());
    }
}