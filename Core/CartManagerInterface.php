<?php

namespace eezeecommerce\CartBundle\Core;


use eezeecommerce\CartBundle\Cart\CartItem;

interface CartManagerInterface
{
    /**
     * Add Item To Cart
     *
     * @param CartItem $item
     * @param int $quantity
     *
     * @return mixed
     */
    public function addItem(EntityInterface $item, $quantity = 1);

    /**
     * Check if item exists
     *
     * @param $itemOrId
     *
     * @return mixed
     */
    public function hasItem($itemOrId);

    /**
     * Remove Item from cart
     *
     * @param $itemOrId
     *
     * @return mixed
     */
    public function removeItem($itemOrId);

    /**
     * Get all items from cart
     *
     * @return mixed
     */
    public function getItems();

    /**
     * Count number of items in cart
     *
     * @return mixed
     */
    public function count();

    /**
     * Get total of cart
     *
     * @return mixed
     */
    public function total();

    /**
     * Get Formatted Total of Cart
     *
     * @return mixed
     */
    public function totalFormatted();

    /**
     * Check if cart is empty
     *
     * @return boolean
     */
    public function isEmpty();

    /**
     * Clear the cart
     *
     * @return mixed
     */
    public function clear();
}