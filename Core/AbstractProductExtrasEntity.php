<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17/05/16
 * Time: 10:45
 */

namespace eezeecommerce\CartBundle\Core;


abstract class AbstractProductExtrasEntity
{
    private $value;

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }
}