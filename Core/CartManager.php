<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09/05/16
 * Time: 11:40
 */

namespace eezeecommerce\CartBundle\Core;


use eezeecommerce\CartBundle\Cart\CartItem;
use eezeecommerce\CartBundle\CartEvents;
use eezeecommerce\CartBundle\Event\FilterItemEvent;
use eezeecommerce\CartBundle\Event\FilterItemsEvent;
use eezeecommerce\CartBundle\Storage\StorageInterface;
use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;
use eezeecommerce\CurrencyBundle\Provider\CurrencyProvider;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CartManager implements CartManagerInterface
{
    const CART = '_cart';

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var CurrencyItem
     */
    protected $currency;

    public function __construct(EventDispatcherInterface $dispatcher, StorageInterface $storage)
    {
        $this->storage = $storage;

        $this->dispatcher = $dispatcher;
    }

    /**
     * Add Item To Cart
     *
     * @param EntityInterface $item
     * @param int $quantity
     *
     * @return mixed
     */
    public function addItem(EntityInterface $entity, $quantity = 1, array $options = null, array $files = null, $variants = null)
    {
        $item = new CartItem($entity, $quantity);

        if (null !== $options) {
            $item->setOptions($options);
        }

        if (null !== $files) {
            $item->setFiles($files);
        }

        if (null !== $variants) {
            $item->setVariant($variants);
        }

        $event = new FilterItemEvent($item);

        $items = $this->getItems();

        $items[] = $item;

        $this->dispatcher->dispatch(CartEvents::CART_ITEM_ADD_INITIALISE, $event);

        if ($event->isPropagationStopped()) {
            return;
        }

        if (null !== ($response = $event->getResponse())) {
            return $response;
        }

        $saveEvent = new FilterItemsEvent($items);

        $this->dispatcher->dispatch(CartEvents::CART_SAVE_INITIALISE, $saveEvent);

        if ($saveEvent->isPropagationStopped()) {
            return;
        }

        $this->storage->set(SELF::CART, $items);

        $this->dispatcher->dispatch(CartEvents::CART_ITEM_ADD_COMPLETE, $event);

        if (null !== ($response = $event->getResponse())) {
            return $response;
        }

        $this->dispatcher->dispatch(CartEvents::CART_SAVE_COMPLETED, $saveEvent);

        return;
    }

    /**
     * Check if item exists
     *
     * @param $itemOrId
     *
     * @return mixed
     */
    public function hasItem($itemOrId)
    {
        $id = $this->getId($itemOrId);

        return array_key_exists($id, $this->getItems());
    }

    /**
     * Update the quantity required of a specified item
     *
     * @param integer $id  Item Array Key
     * @param integer $qty Quantity to update Item
     *
     * @return null|Response
     */
    public function updateQty($id, $qty)
    {
        $qty = (int) $qty;

        if (!$this->hasItem($id)) {
            return;
        }

        if ($qty < 1) {
            $this->removeItem($id);
            return;
        }

        $items = $this->getItems();

        $item = $items[$id];

        $originalQty = $item->getQuantity();

        $item->setQuantity($qty);

        $updateEvent = new FilterItemEvent($item);

        $this->dispatcher->dispatch(CartEvents::CART_ITEM_UPDATE_INITIALISE, $updateEvent);

        if (null !== ($response = $updateEvent->getResponse())) {
            $item->setQuantity($originalQty);
            return $response;
        }

        $items[$id] = $item;

        $event = new FilterItemsEvent($items);
        $this->dispatcher->dispatch(CartEvents::CART_SAVE_INITIALISE, $event);

        if ($event->isPropagationStopped()) {
            return;
        }

        $this->storage->set(SELF::CART, $items);

        $updateEvent = new FilterItemEvent($item);

        $this->dispatcher->dispatch(CartEvents::CART_ITEM_UPDATE_COMPLETE, $updateEvent);

        if (null !== ($response = $updateEvent->getResponse())) {
            return $response;
        }

        $event = new FilterItemsEvent($items);

        $this->dispatcher->dispatch(CartEvents::CART_SAVE_COMPLETED, $event);
    }

    /**
     * Remove Item from cart
     *
     * @param integer|object $itemOrId Object or Item Id
     *
     * @return mixed
     */
    public function removeItem($itemOrId)
    {
        $id = $this->getId($itemOrId);

        if (!$this->hasItem($id)) {
            return;
        }

        $items = $this->getItems();
        $event = new FilterItemEvent($items[$id]);

        $this->dispatcher->dispatch(CartEvents::CART_ITEM_REMOVE_INITIALISE, $event);

        if ($event->isPropagationStopped()) {
            return;
        }

        $items = $this->getItems();
        unset($items[$id]);
        $eventSave = new FilterItemsEvent($items);
        $this->dispatcher->dispatch(CartEvents::CART_SAVE_INITIALISE, $eventSave);

        $this->storage->set(SELF::CART, $items);

        $this->dispatcher->dispatch(CartEvents::CART_SAVE_COMPLETED, $eventSave);

        $this->dispatcher->dispatch(CartEvents::CART_ITEM_REMOVE_COMPLETE, $event);
    }

    /**
     * Get all items from cart
     *
     * @return mixed
     */
    public function getItems()
    {
        return $this->storage->get(SELF::CART, []);
    }

    /**
     * Count number of items in cart
     *
     * @return mixed
     */
    public function count()
    {
        return count($this->getItems());
    }

    /**
     * Get total of cart
     *
     * @return mixed
     */
    public function total()
    {
        if ($this->isEmpty()) {
            return 0;
        }
        $subtotal = [];

        foreach ($this->getItems() as $item) {
            array_push($subtotal, $this->getSubtotal($item));
        }

        return array_sum($subtotal);
    }

    /**
     * Get Total Weight
     *
     * @return float
     */
    public function getWeight()
    {
        if ($this->isEmpty()) {
            return 0;
        }
        $weight = [];

        foreach ($this->getItems() as $item) {
            array_push($weight, $this->getItemWeight($item));
        }

        return array_sum($weight);
    }

    /**
     * Get total Discount
     *
     * @return float
     */
    public function getDiscount()
    {
        if ($this->isEmpty()) {
            return 0;
        }
        $discount = [];

        foreach ($this->getItems() as $item) {
            array_push($discount, $this->getDiscountSubTotal($item));
        }

        return array_sum($discount);
    }

    /**
     * Get Formatted Total of Cart
     *
     * @return mixed
     */
    public function totalFormatted()
    {
        // TODO: Implement totalFormatted() method.
    }

    /**
     * Check if cart is empty
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return ($this->count() === 0);
    }

    /**
     * Clear the cart
     *
     * @return mixed
     */
    public function clear()
    {
        $event = new FilterItemsEvent($this->getItems());

        $this->dispatcher->dispatch(CartEvents::CART_CLEAR_INITIALISE, $event);

        if ($event->isPropagationStopped()) {
            return;
        }

        $this->storage->remove(SELF::CART);

        $this->dispatcher->dispatch(CartEvents::CART_CLEAR_COMPLETED, $event);

    }

    /**
     * Return the Id of the object or the id provided
     *
     * @param integer|object $itemOrId Object or Item of object
     *
     * @return integer
     */
    private function getId($itemOrId)
    {
        return ($itemOrId instanceof EntityInterface)
            ? $itemOrId->getId()
            : $itemOrId;
    }

    /**
     * Returns Subtotal of Specified Cart Item
     *
     * @param CartItem $item Individual Cart Item
     *
     * @return float
     */
    private function getSubTotal(CartItem $item)
    {
        return ($item->getSubtotal());
    }

    /**
     * Returns Weight of Specified Cart Item
     *
     * @param CartItem $item Individual Cart Item
     *
     * @return float
     */
    private function getItemWeight(CartItem $item)
    {
        return $item->getWeight();
    }

    /**
     * Returns Discount Subtotal of Specified Cart Item
     *
     * @param CartItem $item Individual Cart Item
     *
     * @return float
     */
    private function getDiscountSubTotal(CartItem $item)
    {
        return ($item->getDiscount());
    }

    /**
     * Sets Currency via the Currency Provider
     *
     * @param CurrencyProvider $currency Currency Used in cart
     *
     * @return void
     */
    public function setCurrency(CurrencyProvider $currency)
    {
        if (null !== $currencyCode = $this->storage->get("_currency_code")) {
            $this->currency = $currencyCode;
            return;
        }
        $this->currency = $currency->loadCurrency();
    }

    /**
     * Returns currency used
     *
     * @return CurrencyItem
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Returns Currency code used
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currency->getEntity()->getCurrencyCode();
    }

    public function updatePricing()
    {
        $items = $this->getItems();

        $event = new FilterItemsEvent($items);

        $this->dispatcher->dispatch(CartEvents::CART_UPDATE_PRICING_INITIALISED, $event);

        $this->storage->set(SELF::CART, $items);

        $this->dispatcher->dispatch(CartEvents::CART_UPDATE_PRICING_COMPLETED, $event);

    }
}