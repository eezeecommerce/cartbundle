<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09/05/16
 * Time: 11:36
 */

namespace eezeecommerce\CartBundle\Core;


interface EntityInterface
{
    public function getId();

    public function getBasePrice();
}