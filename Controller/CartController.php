<?php

namespace eezeecommerce\CartBundle\Controller;

use eezeecommerce\UploadBundle\Entity\Uploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CartController extends Controller
{
    /**
     * @Route("/", name="_eezeecommerce_cart")
     */
    public function viewAction()
    {
        $cart = $this->get("eezeecommerce.cart");

        return $this->render("@App/Frontend/Checkout/cart.html.twig", ["cart" => $cart]);
    }

    /**
     * @Route("/add", name="_eezeecommerce_cart_add")
     */
    public function addAction(Request $request)
    {
        $pid = $request->request->get("pid");

        if (null === $pid) {
            throw $this->createNotFoundException("A Valid Product Must Be Added to cart");
        }

        $product = $this->getDoctrine()->getRepository("eezeecommerceProductBundle:Product")
            ->findOneById($pid);

        $qty = $request->request->get("quantity");

        if (null === $product) {
            throw $this->createNotFoundException("Product Cannot be found. Please try again");
        }

        $cart = $this->get("eezeecommerce.cart");

        $options = $request->request->get("options") ?: null;

        $files = $request->files->get("options") ?: null;

        if (null !== $files) {
            $em = $this->getDoctrine()->getEntityManager();
            foreach ($files as $key => $value) {
                if (null === $value) {
                    continue;
                }

                $file = new Uploader();
                $file->setImageFile($value);

                $validator = $this->get("validator");
                $errors = $validator->validate($file);

                if (count($errors) > 0) {
                    $this->addFlash("warning", "Your file exceeds 2MB, please upload a smaller image");
                    return $this->redirect($request->server->get("HTTP_REFERER"));
                }

                $em->persist($file);
                $files[$key] = $file;
            }


            $em->flush();
        }

        $variants = $request->request->get("variants") ?: null;

        $response = $cart->addItem($product, $qty, $options, $files, $variants);

        if ($response instanceof RedirectResponse) {
            return $response;
        }

        return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
    }

    /**
     * @Route("/update", name="_eezeecommerce_cart_update")
     */
    public function updateAction(Request $request)
    {
        $data = $request->request->get("qty");

        $cart = $this->get("eezeecommerce.cart");

        foreach ($data as $key => $qty) {

            $response = $cart->updateQty($key, $qty);

            if ($response instanceof RedirectResponse) {
                return $response;
            }
        }

        return $this->redirect($this->generateUrl("_eezeecommerce_cart"));

    }

    /**
     * @Route("/delete", name="_eezeecommerce_cart_remove")
     */
    public function deleteAction(Request $request)
    {
        $id = $request->query->get("id");

        $cart = $this->get("eezeecommerce.cart");

        $response = $cart->removeItem($id);

        if ($response instanceof RedirectResponse) {
            return $response;
        }

        return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
    }

    /**
     * @Route("/discount", name="_eezeecommerce_cart_discount")
     * @Method({"POST"})
     */
    public function addDiscountCodeAction(Request $request)
    {
        if (!$request->request->has("code")) {
            $this->addFlash("warning", "Discount code doesn't exist.");
            return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
        }

        if (null === $request->request->get("code")) {
            $this->addFlash("warning", "No Discount code entered, Please try again.");
            return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
        }

        $code = $request->request->get("code");

        $session = $request->getSession();

        if ($session->has("discount_code")) {
            if ($session->get("discount_code") == $code) {
                $this->addFlash("warning", "Discount code already added");
                return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
            }
        }

        $check = $this->getDoctrine()->getRepository("eezeecommerceDiscountBundle:DiscountCodes")
            ->findOneBy(array("code" => $code));

        if (count($check) < 1) {
            $this->addFlash("warning", "Discount code doesn't exist.");
            return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
        }

        $startDate = $check->getStart();
        $endDate = $check->getEnd();

        $now = new \DateTime();

        if (!($now >= $startDate) || !($now <= $endDate)) {
            $this->addFlash("warning", "Your discount code has expired, please try again");
            return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
        }

        $session->set("discount_code", $code);

        $cart = $this->get("eezeecommerce.cart");
        $cart->updatePricing();

        $this->addFlash("success", "Your discount code was successfully added");
        return $this->redirect($this->generateUrl("_eezeecommerce_cart"));
    }

    /**
     * @Route("/clear")
     */
    public function clearAction(Request $request)
    {

    }
}