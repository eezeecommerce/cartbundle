<?php

/*
 * Copyright EezeeCommerce Ltd 2013-2015
 * Developed By
 * Liam Sorsby
 */

namespace eezeecommerce\CartBundle\Exception;

/**
 * Description of InvalidArgumentException
 *
 * @author Liam Sorsby
 */
class InvalidArgumentException extends \InvalidArgumentException
{
}
