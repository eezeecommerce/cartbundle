<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02/08/16
 * Time: 16:29
 */

namespace eezeecommerce\CartBundle\EventListener;


use eezeecommerce\CartBundle\Core\CartManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class UserEventListener implements EventSubscriberInterface
{
    /**
     * @var CartManager
     */
    protected $manager;

    public function __construct(CartManager $manager)
    {
        $this->manager = $manager;
    }

    public static function getSubscribedEvents()
    {
        return array(
            SecurityEvents::INTERACTIVE_LOGIN => array(
                "onUserLogin", 0
            )
        );
    }

    public function onUserLogin(InteractiveLoginEvent $event)
    {
        $this->manager->updatePricing();
    }
}