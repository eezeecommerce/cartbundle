<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/05/16
 * Time: 15:22
 */

namespace eezeecommerce\CartBundle;


class CartEvents
{
    /**
     * Cart is complete but can be altered prior to completion
     * To be used to recalculate all prices and taxes etc.
     */
    const CART_SAVE_INITIALISE = "eezeecommerce.cart.save.initialise";

    /**
     * Cart has been saved and cannot be altered
     */
    const CART_SAVE_COMPLETED = "eezeecommerce.cart.save.completed";

    /**
     * Cart has started to be cleared but can be altered
     */
    const CART_CLEAR_INITIALISE = "eezeecommerce.cart.clear.initialise";

    /**
     * Cart has been cleared and has completed
     */
    const CART_CLEAR_COMPLETED = "eezeecommerce.cart.clear.completed";

    /**
     * Cart item add initialise
     */
    const CART_ITEM_ADD_INITIALISE = "eezeecommerce.cart_item.add.initialise";

    /**
     * Cart item add complete
     */
    const CART_ITEM_ADD_COMPLETE = "eezeecommerce.cart_item.add.complete";

    /**
     * Cart item add initialise
     */
    const CART_ITEM_UPDATE_INITIALISE = "eezeecommerce.cart_item.update.initialise";

    /**
     * Cart item add complete
     */
    const CART_ITEM_UPDATE_COMPLETE = "eezeecommerce.cart_item.update.complete";

    /**
     * Cart item add error
     */
    const CART_ITEM_ADD_ERROR = "eezeecommerce.cart_item.add.error";

    /**
     * Cart item remove initialise
     */
    const CART_ITEM_REMOVE_INITIALISE = "eezeecommerce.cart_item.remove.initialise";

    /**
     * Cart item remove complete
     */
    const CART_ITEM_REMOVE_COMPLETE = "eezeecommerce.cart_item.remove.complete";

    /**
     * Cart item remove error
     */
    const CART_ITEM_REMOVE_ERROR = "eezeecommerce.cart_item.remove.error";

    const CART_UPDATE_PRICING_INITIALISED = "eezeecommerce.cart.update_pricing.initialised";

    const CART_UPDATE_PRICING_COMPLETED = "eezeecommerce.cart.update_pricing.completed";
}