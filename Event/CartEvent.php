<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19/05/16
 * Time: 14:12
 */

namespace eezeecommerce\CartBundle\Event;


use eezeecommerce\CartBundle\Core\CartManager;
use Symfony\Component\EventDispatcher\Event;

class CartEvent extends Event
{
    protected $manager;

    public function __construct(CartManager $manager)
    {
        $this->manager = $manager;
    }

    public function getCart()
    {
        return $this->manager;
    }
}