<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09/05/16
 * Time: 16:36
 */

namespace eezeecommerce\CartBundle\Event;


use Symfony\Component\EventDispatcher\Event;

class FilterItemsEvent extends Event
{
    protected $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public function getItems()
    {
        return $this->items;
    }
}