<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09/05/16
 * Time: 13:41
 */

namespace eezeecommerce\CartBundle\Event;


use eezeecommerce\CartBundle\Cart\CartItem;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

class FilterItemEvent extends Event
{
    protected $item;

    private $response;

    public function __construct(CartItem $item)
    {
        $this->item = $item;
    }

    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getEntity()
    {
        return $this->item->getEntity();
    }

    public function getItem()
    {
        return $this->item;
    }

    public function getQuantity()
    {
        return $this->item->getQuantity();
    }
}