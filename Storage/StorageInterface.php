<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 09/05/16
 * Time: 11:43
 */

namespace eezeecommerce\CartBundle\Storage;


interface StorageInterface
{
    /**
     * Returns a cart item
     *
     * @param mixec      $name    key for item
     * @param null|mixed $default default mode
     *
     * @return mixed
     */
    public function get($name, $default = null);

    /**
     * Checks if cart item exists
     *
     * @param mixed $name key of cart
     *
     * @return boolean
     */
    public function has($name);

    /**
     * Set item in cart
     *
     * @param mixed $name  key of cart
     * @param mixed $value value of cart item
     *
     * @return mixed
     */
    public function set($name, $value);

    /**
     * Get all data from Cart
     *
     * @return mixed
     */
    public function all();

    /**
     * Remove element from cart
     *
     * @param mixed $name key of cart element being removed
     *
     * @return boolean
     */
    public function remove($name);

    /**
     * Clears Cart
     */
    public function clear();

    /**
     * Save Cart
     *
     * @return void
     */
    public function save();
}