<?php

namespace eezeecommerce\CartBundle\Storage;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionStorage implements StorageInterface
{

    /**
     * Instance of session interface
     *
     * @var SessionInterface
     */
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * Returns a cart item
     *
     * @param mixed      $name    key for item
     * @param null|mixed $default default mode
     *
     * @return mixed
     */
    public function get($name, $default = null)
    {
        return $this->session->get($name, $default);
    }

    /**
     * Checks if cart item exists
     *
     * @param mixed $name key of cart
     *
     * @return boolean
     */
    public function has($name)
    {
        return $this->session->has($name);
    }

    /**
     * Set item in cart
     *
     * @param mixed $name  key of cart
     * @param mixed $value value of cart item
     *
     * @return mixed
     */
    public function set($name, $value)
    {
        return $this->session->set($name, $value);
    }

    /**
     * Get all data from Cart
     *
     * @return mixed
     */
    public function all()
    {
        return $this->session->all();
    }

    /**
     * Remove element from cart
     *
     * @param mixed $name key of cart element being removed
     *
     * @return boolean
     */
    public function remove($name)
    {
        return $this->session->remove($name);
    }

    /**
     * Clears Cart
     */
    public function clear()
    {
        return $this->session->clear();
    }

    /**
     * @inheritdoc
     */
    public function save()
    {
        $this->session->save();
    }
}